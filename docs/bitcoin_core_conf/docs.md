# Wymagania 
--------------------------------------


    - system: Ubuntu (lub inna dystrybucja Linux)
    - python 3
    - bitcoin core
    
# Konfiguracja 
--------------------------------------
    
    - uruchom ./bitcoind -daemon
    - zakończ ./bitcoin-cli stop
    - w katalogu domowym powinien pojawić się folder .bitcoin
    - utwórz w tym katalogu plik bitcoin.conf
    - skopiuj do niego zawartość pliku bitcoin.conf.txt
    - zainstaluj zależności
    - uruchom ./bitcoind -daemon
    - uruchom plik bitcoinrpc.py