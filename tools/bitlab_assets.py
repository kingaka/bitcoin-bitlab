import struct
import socket
import time
import hashlib
import random
import binascii


def discover_node_addresses():
    # list of seeds hardcoded in a Bitcoin
    # source: https://en.bitcoin.it/wiki/Satoshi_Client_Node_Discovery
    seeds_list = [
        ("seed.bitcoin.sipa.be", 8333),
        ("dnsseed.bluematt.me", 8333),
        ("dnsseed.bitcoin.dashjr.org", 8333),
        ("seed.bitcoinstats.com", 8333),
        ("seed.bitnodes.io", 8333),
        ("bitseed.xf2.org", 8333),
    ]
    peers_list = []
    try:
        for (ip_address, port) in seeds_list:
            for complete_info in socket.getaddrinfo(ip_address, port,
                                                    socket.AF_INET, socket.SOCK_STREAM,
                                                    socket.IPPROTO_TCP):
                # print(complete_info) -> print all information
                peers_list.append((complete_info[4][0], complete_info[4][1]))
    except Exception:
        return peers_list


def makeMessage(cmd, payload):
    magic = 0xd9b4bef9
    checksum = hashlib.sha256(hashlib.sha256(payload).digest()).digest()[:4]
    return(struct.pack('L12sL4s', magic, cmd.encode(), len(payload), checksum) + payload)

def create_network_address(ip_address, port):
    network_address = struct.pack('>8s16sH', b'\x01',
        bytearray.fromhex("00000000000000000000ffff") + socket.inet_aton(ip_address), port)
    return(network_address)

def create_sub_version():
    sub_version = "/Satoshi:0.7.2/"
    return b'\x0F' + sub_version.encode()

def versionMessage():
    version = 60002
    services = 1
    timestamp = int(time.time())
    addr_recv = struct.pack("Q", 0)
    addr_recv += create_network_address("127.0.0.1", 8333)
    addr_from = create_network_address(discover_node_addresses()[0][0], 8333)
    nonce = random.getrandbits(64)
    user_agent_bytes = struct.pack("B", 0)
    height = 0
    payload = struct.pack('<LQQ26s26sQ16sL', version, services, timestamp, addr_recv,addr_from, nonce, create_sub_version(),height)
    return payload

def create_message_verack():
    return bytearray.fromhex("f9beb4d976657261636b000000000000000000005df6e0e2")

def version():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print("ip:", discover_node_addresses()[0][0])
    sock.connect((discover_node_addresses()[0][0], 8333))
    request_data = makeMessage("version", versionMessage())
    sock.send(request_data)
    print("version:", binascii.hexlify(request_data))
    response_data = sock.recv(1024)
    request_data = makeMessage("version", create_message_verack())
    print("verack:", binascii.hexlify(request_data))
    response_data = sock.recv(1024)
    sock.close()

def getTxMsg():
    magic = "f9beb4d9"
    peers = discover_node_addresses()
    ip = socket.gethostbyname(peers[0][0])
    port = peers[0][1]
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((ip,port))

    print(binascii.hexlify(makeMessage("tx",versionMessage())))
