import sys

from colorama import init
init(strip=not sys.stdout.isatty()) # strip colors if stdout is redirected
from termcolor import cprint 
from pyfiglet import figlet_format
from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException
from settings import *
from tools.bitlab_assets import *




cprint(figlet_format('Bitlab', font='starwars'))
cprint('Commands: \n')
cprint('getpeers\t version and verack\t\t getblockhash \t getblock\t\n\n ping \t getpeerinfo \t getblockheader \t getTxMsg \n')

option = '';
while option != 'exit':
	option = input("Write option: ")
	if option == 'getpeers':
		print(discover_node_addresses())
	if option == 'version' or option == 'verack':
		version()
	if option == 'getTxMsg':
		getTxMsg()
	if option == 'getblockhash':
		high = int(input('High:'))
		command = [["getblockhash",high]]
		block_hash = rpc_connection.batch_(command)
		print(block_hash)
	if option == 'getblock':
		hash = input('Hash: ')
		command = [["getblock",hash]]
		block = rpc_connection.batch_(command)
		print(block)
	if option == 'ping':
		command = [["getpeerinfo"]]
		block_hash = rpc_connection.batch_(command)
		print(block_hash[0][0]["pingtime"])
	if option == 'getpeerinfo':
		command = [["getpeerinfo"]]
		block_hash = rpc_connection.batch_(command)
		print(block_hash)
	if option == 'getblockheader':
		hash = input('Hash: ')
		command = [["getblockheader",hash]]
		block_hash = rpc_connection.batch_(command)
		print(block_hash)
